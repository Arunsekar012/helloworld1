import { LightningElement } from 'lwc';
export default class HelloWorld extends LightningElement {
  greeting = 'helloWorld';
  changeHandler(event) {
    this.greeting = event.target.value;
  }
}